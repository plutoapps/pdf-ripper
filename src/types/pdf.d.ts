
type Page = Paragraph[];

type Paragraph = {
    str: string
    width: number
    height: number
    transform: number[]
    para: string
    short: string
    font: string
}

type RipperDocument = {
    pages: Page[],
    title: string
}
