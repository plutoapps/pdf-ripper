import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router';

import PDFLoad from './components/PDFLoad.vue';
import PDFView from './components/PDFView.vue';

const routes = [
    { path: '/', component: PDFLoad },
    { path: "/view", name: "view", component: PDFView, props: true }
]

const router = createRouter({
    history: createWebHistory(import.meta.env.VITE_BASE_PATH),
    routes,
});

const app = createApp(App)
app.use(router)
app.mount('#app');
