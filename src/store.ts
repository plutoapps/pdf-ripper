import { reactive } from "vue";

const localStorage = window.localStorage;
const firstLoadTitleString = localStorage.getItem('title');
const firstLoadTitle = firstLoadTitleString ? JSON.parse(firstLoadTitleString) : {};
const firstLoadLastDocument = localStorage.getItem('lastDocument') || "";
const firstLoadPagesString = firstLoadLastDocument ? localStorage.getItem(firstLoadLastDocument) : null;
const firstLoadPages: Page[] = firstLoadPagesString ? JSON.parse(firstLoadPagesString) : [];

type State = {
    pages: Page[]
    title: { [key: string]: string }
    lastDocument: string
}

const state = reactive<State>({
    pages: firstLoadPages,
    title: firstLoadTitle,
    lastDocument: firstLoadLastDocument
});

export default state;

export function pagesKey(title: string) {
    return 'pages-' + encodeURI(title);
}

export function insertDocument(doc: RipperDocument) {
    const key = pagesKey(doc.title);

    state.pages = doc.pages;
    state.title = {
        ...state.title,
        [key]: doc.title
    };
    state.lastDocument = key;

    localStorage.setItem(key, JSON.stringify(doc.pages));
    localStorage.setItem('title', JSON.stringify(state.title));
    localStorage.setItem('lastDocument', key);
}

export function loadDocumentFromStorage(key: string): boolean {
    const documentString = localStorage.getItem(key);
    const documentPages: Page[] = documentString ? JSON.parse(documentString) : [];
    if (documentPages) {
        state.pages = documentPages;
        state.lastDocument = key;
        localStorage.setItem('lastDocument', key);
        return true;
    }
    return false;
}

export function removeDocument(key: string) {

    const _title = { ...state.title };
    delete _title[key];

    state.title = { ..._title };
    state.lastDocument = "";

    localStorage.removeItem(key);
    localStorage.setItem('title', JSON.stringify(state.title));
    localStorage.removeItem('lastDocument');
}

export function clearStorage() {
    localStorage.clear();
}