/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

import { insertDocument } from "../store";

declare const pdfjsLib: any;

export default function load(buffer: ArrayBuffer, title = ""): Promise<RipperDocument> {
    const result: RipperDocument = {
        pages: [],
        title
    }
    const loadingTask = pdfjsLib.getDocument(buffer);
    return loadingTask.promise
        .then(function (doc: any) {
            const numPages = doc.numPages;
            console.log("# Document Loaded");
            console.log("Number of Pages: " + numPages);

            let lastPromise; // will be used to chain promises
            lastPromise = doc.getMetadata().then(function (data: any) {
                if (data.metadata) {
                    const metadata = data.metadata.getAll();
                    result.title = metadata['dc:title'] || title;
                }
            });

            const loadPage = function (pageNum: number) {
                return doc.getPage(pageNum).then(async function (page: any) {
                    return page
                        .getTextContent()
                        .then(function (content: any) {
                            const paragraph: Paragraph[] = [];
                            let lastBlock: Paragraph = {
                                str: "",
                                width: 0,
                                height: 0,
                                transform: [],
                                para: "",
                                short: "",
                                font: ""
                            };
                            let lastParagraph = "";
                            for (let i = 0; i < content.items.length; i++) {
                                const block = content.items[i];

                                // font size change
                                if (block.transform[0] != lastBlock.transform[0]) {
                                    paragraph.push({
                                        ...lastBlock, height: lastBlock.transform[0],
                                        para: lastParagraph,
                                        short: shortParagraph(lastParagraph)
                                    });
                                    lastBlock = block;
                                    lastParagraph = "";
                                }

                                //paragraph
                                else if (lastParagraph != "" &&
                                    block.str == "" &&
                                    block.hasEOL &&
                                    block.transform[4] != lastBlock.transform[4]) {
                                    paragraph.push({
                                        ...lastBlock, height: lastBlock.transform[0],
                                        para: lastParagraph,
                                        short: shortParagraph(lastParagraph)
                                    });
                                    lastBlock = block;
                                    lastParagraph = "";
                                    continue;
                                }

                                lastParagraph += (lastParagraph && block.str ? " " : "") + block.str;
                                lastBlock = block;
                            }


                            if (lastParagraph != "") {
                                paragraph.push({
                                    ...lastBlock,
                                    para: lastParagraph,
                                    short: shortParagraph(lastParagraph)
                                });
                            }

                            page.cleanup();
                            result.pages.push(paragraph);
                        });
                });
            };

            for (let i = 1; i <= numPages; i++) {
                lastPromise = lastPromise.then(loadPage.bind(null, i));
            }
            return lastPromise;
        })
        .then(
            function () {
                console.log("# End of Document");

                insertDocument(result);

                return result;
            },
            function (err: any) {
                console.error("Error: " + err);
            }
        );
};

const shortParagraphLength = 30;
function shortParagraph(source: string): string {
    return source.length > shortParagraphLength ?
        source.slice(0, shortParagraphLength) + '...' :
        source;
}